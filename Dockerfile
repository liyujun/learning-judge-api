FROM python:3.7.4

WORKDIR /app
COPY requirements.txt /app
RUN pip install --no-cache --upgrade -r /app/requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple/

COPY . /app/
CMD ["uvicorn", "api.main:app", "--host", "0.0.0.0", "--port", "80"]
