from datetime import datetime
from typing import Optional, List

from pydantic import BaseModel

from api.models.train import TrainStatus
from api.schemas.prediction import PredictionSchema


class TrainSchema(BaseModel):
    id: int
    train_dataset_file_location: str
    validation_dataset_file_location: str
    pkl_file_location: Optional[str]
    # score: str
    status: TrainStatus

    startedAt: Optional[datetime]
    endedAt: Optional[datetime]
    createdAt: Optional[datetime]
    updatedAt: Optional[datetime]
    
    predictions: Optional[List[PredictionSchema]]

    class Config:
        from_attributes = True
        use_enum_values = True

