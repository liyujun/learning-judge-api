from typing import List, Optional
from datetime import datetime

from pydantic import BaseModel

from api.schemas.train import TrainSchema


class ModelSchema(BaseModel):
    id: int
    name: str
    description: Optional[str] = None
    
    createdAt: Optional[datetime] = None
    updatedAt: Optional[datetime] = None

    trains: Optional[List[TrainSchema]]

    class Config:
        from_attributes = True


class ModelCreateSchema(BaseModel):
    name: str
    description: Optional[str]
