from datetime import datetime
from typing import Optional
from pydantic import BaseModel

from api.models.prediction import PredictionStatus


class PredictionSchema(BaseModel):
    id: int
    test_file_location: str
    result_file_location: Optional[str]
    status: PredictionStatus

    createdAt: Optional[datetime]
    updatedAt: Optional[datetime]

    class Config:
        from_attributes = True
        use_enum_values = True

