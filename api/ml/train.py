import numpy as np
import pandas as pd
import joblib
from lightgbm import LGBMClassifier
from sklearn.metrics import f1_score
from sklearn.metrics import classification_report
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MaxAbsScaler
from sklearn.preprocessing import PowerTransformer


def train_lgbm(path_train, path_test, path_save):
    '''
    训练函数
    :param path_train: 训练集的路径
    :param path_test: 验证集的路径
    :param path_save: 模型的保存路径
    :return:
    '''
    drop_feature = ["feature100","feature77","feature57","feature54","feature60"]

    df = pd.read_csv(path_train, index_col=0) # 读取训练集
    df_test = pd.read_csv(path_test, index_col=0) # 读取验证集
    df_test = df_test.drop(drop_feature,axis=1)
    ## 划分特征和标签
    X_train = df.iloc[:,:-1]
    y_train = df.iloc[:,-1].astype(int)
    X_test = df_test.iloc[:,:-1]
    y_test = df_test.iloc[:,-1].astype(int)

    # 标准化
    standar_feature = ["feature44","feature14","feature103","feature70","feature72","feature82","feature30","feature61",
                               "feature31","feature66","feature45","feature28","feature62","feature104","feature20","feature75",
                               "feature95","feature7","feature83","feature79", "feature1","feature2","feature13","feature38","feature56",
                               "feature71","feature4","feature21","feature23","feature63","feature52","feature102","feature0"]
    std = StandardScaler(with_mean=False)#实例化
    std.fit(X_train[standar_feature])#训练
    X_train[standar_feature] = std.transform(X_train[standar_feature])#转化或预测predict
    std2 = StandardScaler(with_mean=False)#实例化
    std2.fit(X_test[standar_feature])#训练
    X_test[standar_feature] = std2.transform(X_test[standar_feature])


    # 可行！
    # MaxAbsScaler 变换
    feature_MaxAbsScaler = ["feature85","feature35","feature58","feature27","feature11","feature16","feature22","feature29",
                            "feature32","feature34","feature94","feature41","feature48","feature42","feature89","feature53",]
    mas = MaxAbsScaler()
    mas.fit(X_train[feature_MaxAbsScaler])
    X_train[feature_MaxAbsScaler] = mas.transform(X_train[feature_MaxAbsScaler])
    mas2 = MaxAbsScaler()
    mas2.fit(X_test[feature_MaxAbsScaler])
    X_test[feature_MaxAbsScaler] = mas2.transform(X_test[feature_MaxAbsScaler])


    # PowerTransformer变换
    feature_PowerTransformer = ["feature55","feature9","feature47","feature3","feature6","feature8","feature17","feature18",
                                "feature19","feature24","feature25","feature26","feature39","feature40","feature43","feature50",
                                "feature65","feature51","feature67","feature69","feature73","feature74","feature78","feature80",
                                "feature81","feature84","feature86","feature87","feature88","feature91","feature96","feature98",
                                "feature99","feature101","feature76",]
    # print("当前的feature_PowerTransformer：",feature_PowerTransformer)
    # method='Yeo-Johnson', standardize=False
    pt = PowerTransformer(method='yeo-johnson', standardize=False)
    pt.fit(X_train[feature_PowerTransformer])
    X_train[feature_PowerTransformer] = pt.transform(X_train[feature_PowerTransformer])
    pt2 = PowerTransformer(method='yeo-johnson', standardize=False)
    pt2.fit(X_test[feature_PowerTransformer])
    X_test[feature_PowerTransformer] = pt2.transform(X_test[feature_PowerTransformer])

    # 构建和训练模型
    clf = LGBMClassifier(n_estimators=108, learning_rate=0.58)  # , learning_rate=0.05
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    # 计算评估指标F1
    fl_score_reult = f1_score(y_test, y_pred, average='macro')
    print('F1_socre:{}'.format(fl_score_reult))
    print(classification_report(y_test, y_pred))

    # 保存模型
    joblib.dump(clf, path_save)
    print("训练完毕，模型已保存！")

if __name__ == '__main__':
    path_train = "./训练数据集/train_0531_oversample.csv"
    path_test = "./训练数据集/validate_1000.csv"
    path_save = "model/clf_lgmb.pkl"

    train_lgbm(path_train=path_train, path_test=path_test, path_save=path_save)
