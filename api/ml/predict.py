import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MaxAbsScaler
from sklearn.preprocessing import PowerTransformer
import json
import joblib

def predict_lgbm(path_model, path_test, path_label):
    # 读取模型
    clf_lgmb = joblib.load(path_model) # 导入模型

    # 对测试集做数据预处理
    X_test = pd.read_csv(path_test, index_col=0) # 读取测试集
    drop_feature = ["feature100","feature77","feature57","feature54","feature60"]
    X_test = X_test.drop(drop_feature,axis=1)

    standar_feature = ["feature44","feature14","feature103","feature70","feature72","feature82","feature30","feature61",
                               "feature31","feature66","feature45","feature28","feature62","feature104","feature20","feature75",
                               "feature95","feature7","feature83","feature79", "feature1","feature2","feature13","feature38","feature56",
                               "feature71","feature4","feature21","feature23","feature63","feature52","feature102","feature0"]
    std2 = StandardScaler(with_mean=False)#实例化
    std2.fit(X_test[standar_feature])#训练
    X_test[standar_feature] = std2.transform(X_test[standar_feature])
    # MaxAbsScaler 变换
    feature_MaxAbsScaler = ["feature85","feature35","feature58","feature27","feature11","feature16","feature22","feature29",
                            "feature32","feature34","feature94","feature41","feature48","feature42","feature89","feature53",]
    mas2 = MaxAbsScaler()
    mas2.fit(X_test[feature_MaxAbsScaler])
    X_test[feature_MaxAbsScaler] = mas2.transform(X_test[feature_MaxAbsScaler])
    # PowerTransformer变换
    feature_PowerTransformer = ["feature55","feature9","feature47","feature3","feature6","feature8","feature17","feature18",
                                "feature19","feature24","feature25","feature26","feature39","feature40","feature43","feature50",
                                "feature65","feature51","feature67","feature69","feature73","feature74","feature78","feature80",
                                "feature81","feature84","feature86","feature87","feature88","feature91","feature96","feature98",
                                "feature99","feature101","feature76",]
    # method='Yeo-Johnson', standardize=False
    pt2 = PowerTransformer(method='yeo-johnson', standardize=False)
    pt2.fit(X_test[feature_PowerTransformer])
    X_test[feature_PowerTransformer] = pt2.transform(X_test[feature_PowerTransformer])
    # 对测试集进行预测
    y_pred = clf_lgmb.predict(X_test)
    sample_id = np.array(X_test.index, dtype=str)
    # 将y_pred的元素类型转换为python原生的int类型
    y_pred = list(y_pred)
    for i in range(len(y_pred)):
        y_pred[i] = int(y_pred[i])
    # 将sample_id与预测结果对应，生成字典
    result_dict = dict(zip(sample_id, y_pred))
    # 字典转json
    result_json = json.dumps(result_dict)
    # 保存json文件
    with open(path_label, 'w', encoding='utf-8') as f:
        f.write(result_json)
    print("预测完毕！结果已保存")

if __name__ == '__main__':
    path_model = "model/clf_lgmb.pkl"
    path_test = "./测试集0705/测试集/test_2000_x.csv"
    path_label = r"submit_result/猪在河里_深圳信息职业技术学院.json"
    predict_lgbm(path_model=path_model, path_test=path_test, path_label=path_label)