import shutil
from os import path, getcwd
from uuid import uuid4
from typing import List

from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException, UploadFile, status
from fastapi.responses import FileResponse
from sqlalchemy.orm import Session

from api.database import get_db
from api.models.prediction import Prediction, PredictionStatus
from api.models.train import Train
from api.schemas.prediction import PredictionSchema
from api.ml.predict import predict_lgbm


router = APIRouter()


def do_prediction(prediction: Prediction, db: Session):
    # 更新预测任务状态
    db.query(Prediction).filter(Prediction.id == prediction.id).update(
        {"status": PredictionStatus.on_progress}
    )
    db.commit()

    # 执行预测任务
    path_model = prediction.train.pkl_file_location
    path_test = prediction.test_file_location
    path_label = f"upload_files/{uuid4()}"

    try:
        print("正在执行")
        predict_lgbm(path_model=path_model, path_test=path_test, path_label=path_label)
        # 更新预测任务状态
        db.query(Prediction).filter(Prediction.id == prediction.id).update(
            {
                "status": PredictionStatus.done,
                "result_file_location": path_label
            }
        )
        db.commit()
        print("执行成功")
    except Exception as e:
        print(f"执行失败: {e}")
        db.query(Prediction).filter(Prediction.id == prediction.id).update(
            {"status": PredictionStatus.error}
        )
        db.commit()


@router.get("/{prediction_id}/start", status_code=status.HTTP_202_ACCEPTED)
def start_predict(
    prediction_id: int,
    background_tasks: BackgroundTasks,
    db: Session = Depends(get_db)
):
    prediction = db.query(Prediction).filter(Prediction.id == prediction_id).one_or_none()
    if not prediction:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"找不到 id 为 {prediction_id} 的预测任务",
        )

    if prediction.status is PredictionStatus.on_progress:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=f"该预测任务正在进行中"
        )

    print(prediction.train.pkl_file_location)
    if prediction.train.pkl_file_location == "":
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=f"该训练任务没有产生模型，无法进行预测"
        )

    background_tasks.add_task(
        do_prediction,
        prediction,
        db
    )


@router.get(
    "/{prediction_id}/result", description="获取预测结果文件", status_code=status.HTTP_200_OK
)
def get_result(prediction_id: int, db: Session = Depends(get_db)):
    prediction = db.query(Prediction).filter(Prediction.id == prediction_id).one_or_none()
    if not prediction:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"无法找到 id 为 {prediction_id} 的预测任务",
        )
    if str(prediction.result_file_location) == "":
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="该预测任务还没有结果集"
        )

    return FileResponse(str(prediction.result_file_location))


@router.get(
    "",
    description="获取所有预测任务",
    status_code=status.HTTP_200_OK,
    response_model=List[PredictionSchema],
)
def get_predictions(db: Session = Depends(get_db)):
    predictions = db.query(Prediction).all()
    return predictions


@router.get(
    "/{prediction_id}",
    description="获取预测任务",
    status_code=status.HTTP_200_OK,
    response_model=PredictionSchema,
)
def get_prediction(prediction_id: int, db: Session = Depends(get_db)):
    prediction = (
        db.query(Prediction).filter(Prediction.id == prediction_id).one_or_none()
    )
    if not prediction:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"找不到 id 为 {prediction_id} 的预测任务",
        )
    return prediction


@router.post(
    "",
    description="创建预测任务",
    status_code=status.HTTP_201_CREATED,
    response_model=PredictionSchema,
)
def create_prediction(
        train_id: int,
        test_file: UploadFile,
        db: Session = Depends(get_db)
    ):
    train = db.query(Train).filter(Train.id == train_id).one_or_none()
    if not train:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"找不到 id 为 {train_id} 的训练任务",
        )

    if test_file.size == 0:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="预测数据集文件大小不能为 0"
        )

    test_file_location = f"upload_files/{uuid4()}"
    with open(path.join(getcwd(), test_file_location), "wb") as f:
        shutil.copyfileobj(test_file.file, f)

    new_prediction = Prediction(
        train=train,
        test_file_location=test_file_location
    )
    db.add(new_prediction)
    db.commit()
    db.refresh(new_prediction)
    return new_prediction


@router.delete(
    "/{prediction_id}", status_code=status.HTTP_204_NO_CONTENT, description="删除预测任务"
)
def delete_prediction(prediction_id: int, db: Session = Depends(get_db)):
    prediction = db.query(Prediction).filter(Prediction.id == prediction_id)
    if not prediction.first():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"找不到 id 为 {prediction_id} 的预测任务",
        )
    prediction.delete(synchronize_session=False)
