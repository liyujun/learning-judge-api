from typing import List
from typing_extensions import Annotated

from fastapi import Body, status, APIRouter, HTTPException, Depends
from sqlalchemy.orm import Session

from api.database import get_db
from api.schemas.model import ModelSchema, ModelCreateSchema
from api.models.model import Model


router = APIRouter()


@router.get(
    "/{model_id}",
    description="获取模型",
    status_code=status.HTTP_200_OK,
    response_model=ModelSchema
)
def get_model(model_id: int, db: Session = Depends(get_db)):
    model = db.query(Model).filter(Model.id == model_id).one_or_none()
    if not model:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"找不到 id 为 {model_id} 的模型"
        )
    return model


@router.get(
    "",
    description="获取所有模型",
    status_code=status.HTTP_200_OK,
    response_model=List[ModelSchema]
)
def get_all_models(db: Session = Depends(get_db)):
    models = db.query(Model).all()
    print(models)
    return models


@router.post(
    "",
    description="创建模型",
    status_code=status.HTTP_201_CREATED,
    response_model=ModelSchema,
)
def create_model(
        payload: Annotated[
            ModelCreateSchema,
            Body(
                examples=[
                    {
                        "name": "测试模型",
                        "description": "该模型用于测试"
                    }
                ]
            )
        ],
        db: Session=Depends(get_db)
    ):
    print(payload)
    new_model = Model(
        name=payload.name,
        description=payload.description
    )
    db.add(new_model)
    db.commit()
    db.refresh(new_model)
    return new_model


@router.delete(
    "/{model_id}",
    description="删除模型",
    status_code=status.HTTP_204_NO_CONTENT
)
def delete_model(model_id: int, db: Session = Depends(get_db)):
    model = db.query(Model).filter(Model.id == model_id).one_or_none()

    if model:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"找不到 id 为 {model_id} 的模型"
        )

    db.delete(model)
    db.commit()
