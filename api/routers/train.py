from datetime import datetime
import shutil
from os import getcwd, path
from typing import List
from uuid import uuid4

from fastapi import APIRouter, BackgroundTasks, Depends, UploadFile, status, HTTPException
from sqlalchemy.orm import Session

from api.database import get_db
from api.models.train import Train, TrainStatus
from api.models.model import Model
from api.schemas.train import TrainSchema
from api.ml.train import train_lgbm


router = APIRouter()


def do_train(train: Train, db: Session):
    # 更新训练状态
    db.query(Train).filter(Train.id == train.id).update(
        {
            "status": TrainStatus.on_progress,
            "startedAt": datetime.now(),
            "endedAt": None
        }
    )
    db.commit()
    
    pkl_file_location = f"upload_files/{uuid4()}"
    try:
        train_lgbm(
            path_train=train.train_dataset_file_location,
            path_test=train.validation_dataset_file_location,
            path_save=pkl_file_location
        )
        db.query(Train).filter(Train.id == train.id).update(
            {
                "status": TrainStatus.done,
                "pkl_file_location": pkl_file_location,
                "endedAt": datetime.now()
            }
        )
        db.commit()

    except:
        # 训练失败
        db.query(Train).filter(Train.id == train.id).update(
            {
                "status": TrainStatus.error,
                "endedAt": datetime.now()
            }
        )
        db.commit()


@router.post(
    "/{train_id}/start",
    description="开始训练",
    status_code=status.HTTP_202_ACCEPTED
)
def start_train(
    train_id: int,
    background_tasks: BackgroundTasks,
    db: Session = Depends(get_db)
):
    train = db.query(Train).filter(Train.id == train_id).one_or_none()
    if not train:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"找不到 id 为 {train_id} 的训练"
        )
    if train.status is TrainStatus.on_progress:
        raise HTTPException(status_code=status.HTTP_406_NOT_ACCEPTABLE, detail="正在训练中")

    background_tasks.add_task(do_train, train, db)


@router.get(
    "/{train_id}",
    description="获取训练任务",
    status_code=status.HTTP_200_OK,
    response_model=TrainSchema,
)
def get_train(train_id: int, db: Session = Depends(get_db)):
    train = db.query(Train).filter(Train.id == train_id).one_or_none()
    if not train:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"找不到 id 为 {train_id} 的训练"
        )
    return train


@router.post(
    "",
    description="创建训练",
    status_code=status.HTTP_200_OK,
    response_model=TrainSchema
)
def create_train(
    model_id: int,
    train_dataset_file: UploadFile,
    validation_dataset_file: UploadFile,
    db: Session = Depends(get_db),
):
    model = db.query(Model).filter(Model.id == model_id).one_or_none()
    if not model:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"找不到 id 为 {model_id} 的模型",
        )

    if train_dataset_file.size == 0:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="训练集文件 大小不能为 0"
        )

    if validation_dataset_file.size == 0:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="验证集文件 大小不能为 0"
        )

    train_dataset_file_location = f"upload_files/{uuid4()}"
    with open(path.join(getcwd(), train_dataset_file_location), "wb") as f:
        shutil.copyfileobj(train_dataset_file.file, f)

    validation_dataset_file_location = f"upload_files/{uuid4()}"
    with open(path.join(getcwd(), validation_dataset_file_location), "wb") as f:
        shutil.copyfileobj(validation_dataset_file.file, f)

    new_train = Train(
        model=model,
        status=TrainStatus.not_started,
        train_dataset_file_location=train_dataset_file_location,
        validation_dataset_file_location=validation_dataset_file_location,
    )
    db.add(new_train)
    db.commit()
    db.refresh(new_train)
    return new_train


@router.get(
    "",
    description="获取所有训练",
    status_code=status.HTTP_200_OK,
    response_model=List[TrainSchema],
)
def get_trains(db: Session = Depends(get_db)):
    trains = db.query(Train).all()
    return trains


@router.delete(
    "/{train_id}",
    description="删除训练任务",
    status_code=status.HTTP_204_NO_CONTENT
)
def delete_train(train_id: int, db: Session = Depends(get_db)):
    train = db.query(Train).filter(Train.id == train_id).one_or_none()
    if not train:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"无法找到 id 为 {train_id} 的训练任务"
        )
    db.delete(train)
    db.commit()

