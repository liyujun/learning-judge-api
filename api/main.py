import os

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import uvicorn

from api.database import Base, engine
from api.routers.model import router as model_router
from api.routers.train import router as train_router
from api.routers.prediction import router as prediction_router


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)
app.include_router(model_router, prefix="/models")
app.include_router(train_router, prefix="/trains")
app.include_router(prediction_router, prefix="/predictions")


@app.on_event("startup")
def startup():
    Base.metadata.create_all(engine)
    if not os.path.exists("upload_files"):
        os.mkdir("upload_files")


@app.get("/")
def root():
    return {"message": "Hello world"}

def dev():
    uvicorn.run("api.main:app", reload=True, host="0.0.0.0", port=8000)

def prod():
    uvicorn.run("api.main:app", host="0.0.0.0", port=80)

if __name__ == "__main__":
    prod()

