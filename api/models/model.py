from sqlalchemy import TIMESTAMP, Integer, String, Column, Text 
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from api.database import Base


class Model(Base):
    __tablename__ = "model"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False, comment="模型名称")
    description = Column(Text, nullable=True, comment="模型描述")

    createdAt = Column(
        TIMESTAMP(timezone=True), nullable=False, server_default=func.now()
    )
    updatedAt = Column(TIMESTAMP(timezone=True), default=None, onupdate=func.now())

    trains = relationship("Train", back_populates="model", cascade="all, delete")

