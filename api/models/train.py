import enum

from sqlalchemy import TIMESTAMP, ForeignKey, Integer, String, Column, Enum
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from api.database import Base


class TrainStatus(enum.Enum):
    not_started = "尚未开始"
    on_progress = "正在进行中"
    error = "失败"
    done = "已结束"


class Train(Base):
    __tablename__ = "train"
    id = Column(Integer, primary_key=True, index=True)
    train_dataset_file_location = Column(String, nullable=False, comment="训练集文件地址")
    validation_dataset_file_location = Column(String, nullable=False, comment="验证集文件地址")
    pkl_file_location = Column(String, nullable=True, comment="模型文件地址")
    # score = Column(String, nullable=False, comment="评估指标")
    status = Column(
        "value", Enum(TrainStatus), default=TrainStatus.not_started, comment="训练状态"
    )
    startedAt = Column(TIMESTAMP(timezone=True), default=None, comment="开始时间")
    endedAt = Column(TIMESTAMP(timezone=True), default=None, comment="结束时间")

    createdAt = Column(
        TIMESTAMP(timezone=True), nullable=False, server_default=func.now()
    )
    updatedAt = Column(TIMESTAMP(timezone=True), default=None, onupdate=func.now())

    model_id = Column(Integer, ForeignKey("model.id", ondelete="CASCADE"))
    model = relationship("Model", back_populates="trains", cascade="all, delete")
    predictions = relationship("Prediction", back_populates="train", cascade="all, delete")

