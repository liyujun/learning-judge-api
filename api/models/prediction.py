import enum

from sqlalchemy import TIMESTAMP, Column, Enum, ForeignKey, Integer, String, func
from sqlalchemy.orm import relationship

from api.database import Base


class PredictionStatus(enum.Enum):
    not_started = "尚未开始"
    on_progress = "正在进行中"
    error = "失败"
    done = "结束"


class Prediction(Base):
    __tablename__ = "prediction"
    id = Column(Integer, primary_key=True, index=True)
    result_file_location = Column(String, nullable=True, comment="预测结果文件地址")
    test_file_location = Column(String, nullable=False, comment="预测数据集文件地址")
    status = Column(
        "value",
        Enum(PredictionStatus),
        default=PredictionStatus.not_started,
        comment="预测状态",
    )

    createdAt = Column(
        TIMESTAMP(timezone=True), nullable=False, server_default=func.now()
    )
    updatedAt = Column(TIMESTAMP(timezone=True), default=None, onupdate=func.now())

    train_id = Column(Integer, ForeignKey("train.id", ondelete="CASCADE"))
    train = relationship("Train", back_populates="predictions", cascade="all, delete")

