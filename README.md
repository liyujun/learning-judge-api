# Learning Judge API

## 前置要求

1. 准备 Python 3.7.4 环境
2. 下载 [Poetry](https://python-poetry.org/docs/#installation)

## 开发

- 安装依赖

```bash
poetry install
```

- 运行

```bash
poetry run dev
```

- 导出依赖

```bash
poetry export --without-hashes --format=requirements.txt > requirements.txt
```

## 构建

```bash
docker build -t learning-judge-api:latest .
```

## 部署

```bash
docker run -itd -p 8080:80 --name learning-judge-api learning-judge-api:latest

# 查看日志
docker logs -f learning-judge-api
```

### 查看 API 文档

访问 http://localhost:8080/docs

